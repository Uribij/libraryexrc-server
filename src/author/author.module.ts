import { Module } from '@nestjs/common';
import { MongoConectorModule } from 'src/mongo-conector/mongo-conector.module';
import { AuthorController } from './author.controller';
import { AuthorRepository } from './author.repository';
import { AuthorService } from './author.service';

@Module({
  controllers: [AuthorController],
  providers: [AuthorService,AuthorRepository],
  imports:[MongoConectorModule]})
export class AuthorModule {}
