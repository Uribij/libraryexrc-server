import { HttpException, Injectable } from '@nestjs/common';
import { AuthorDto } from './author.dto';
import { Author } from './author.interface';
import { AuthorRepository } from './author.repository';

@Injectable()
export class AuthorService {
  constructor(private readonly authorRepository: AuthorRepository) {}

  getAllAuthors(): Promise<AuthorDto[]> {
    return this.authorRepository.getAllAuthors();
  }

  getAuthorById(authorId: number): Promise<AuthorDto> {
    return this.authorRepository.getAuthorById(+`${authorId}`);
  }

  updateAuthor(author: AuthorDto) {
    if (!this.verifyAuthor(author.authorId)) {
      throw new HttpException(
        'The author you are trying to update does no exist!',
        404,
      );
    }
    return this.authorRepository.updateAuthor(author);
  }

  addAuthor(author: AuthorDto): Promise<AuthorDto> {
    author.authorId = +`${author.authorId}`;
    if (this.authorRepository.addAuthor(author)) {
      return this.getAuthorById(author.authorId);
    }
  }

  async deleteAuthor(authorId: number): Promise<boolean> {
    if (!this.verifyAuthor(+`${authorId}`)) {
      throw new HttpException(
        'The author you are trying to delete does no exist!',
        404,
      );
    }
    const author = await this.getAuthorById(+`${authorId}`);
    return this.authorRepository.deleteAuthor((author as Author)._id);
  }

  verifyAuthor(id: number): boolean {
    return !!this.getAuthorById(id);
  }

  async updateAuthorData(
    authorId: number,
    replacementData: { [propertyName: string]: string | number },
  ): Promise<AuthorDto> {
    if (!this.verifyAuthor(authorId)) {
      throw new HttpException(
        'The author you are trying to update does not exist!',
        404,
      );
    } else {
      return await this.authorRepository.updateAuthorData(
        +`${authorId}`,
        replacementData,
      );
    }
  }
}
