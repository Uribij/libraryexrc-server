import { Inject, Injectable } from '@nestjs/common';
import { Db, ObjectId } from 'mongodb';
import { AuthorDto } from './author.dto';
import { Author } from './author.interface';

@Injectable()
export class AuthorRepository {
  constructor(
    @Inject('DATABASE_CONNECTION')
    private db: Db,
  ) {}

  private readonly COLLECTION_NAME = 'authors';

  async getAllAuthors(): Promise<AuthorDto[]> {
    return await this.db.collection(this.COLLECTION_NAME).find({}).toArray();
  }

  async getAuthorById(authorId: number): Promise<AuthorDto> {
    return {
      ...(await this.db.collection(this.COLLECTION_NAME).findOne({ authorId })),
    };
  }

  async addAuthor(author: AuthorDto): Promise<boolean> {
    return !!(
      await this.db.collection(this.COLLECTION_NAME).insertMany([author])
    ).result.ok;
  }

  async updateAuthor(author: AuthorDto): Promise<AuthorDto> {
    return (
      await this.db.collection(this.COLLECTION_NAME).findOneAndUpdate(
        { _id: author['_id'] },
        {
          $set: { authorName: author.authorName, authorId: author.authorId },
        },
        { returnOriginal: false },
      )
    ).value as AuthorDto;
  }

  async deleteAuthor(_id: ObjectId): Promise<boolean> {
    return !!(
      await this.db.collection(this.COLLECTION_NAME).deleteMany({ _id })
    ).result.ok;
  }

  async updateAuthorData(
    authorId: number,
    pairValueSet: { [propertyName: string]: number | string },
  ): Promise<AuthorDto> {
    return (
      await this.db
        .collection(this.COLLECTION_NAME)
        .findOneAndUpdate(
          { authorId },
          { $set: pairValueSet },
          { returnOriginal: false },
        )
    ).value as AuthorDto;
  }
}
