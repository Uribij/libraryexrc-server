import { ObjectId } from "mongodb";

export interface Author{
    _id:ObjectId;
    authorName:string;
    authorId:number;
}