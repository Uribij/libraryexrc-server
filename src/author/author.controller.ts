import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { AuthorDto } from './author.dto';
import { AuthorService } from './author.service';

@Controller('author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  @Get()
  getAllAuthors(): Promise<AuthorDto[]> {
    return this.authorService.getAllAuthors();
  }

  @Get('/:id')
  getAuthorById(@Param('id') id: number): Promise<AuthorDto> {
    return this.authorService.getAuthorById(+id);
  }

  @Delete('/:id')
  deleteAuthor(@Param('id') id: number): Promise<boolean> {
    return this.authorService.deleteAuthor(+id);
  }

  @Post()
  addAuthor(@Body() author: AuthorDto): Promise<AuthorDto> {
    return this.authorService.addAuthor(author);
  }

  @Put()
  updateAuthor(@Body() author: AuthorDto): Promise<AuthorDto> {
    return this.authorService.updateAuthor(author);
  }

  @Put('/:id')
  updateAuthorData(
    @Param('id') id: number,
    @Body()
    replacementData: { [propertyName: string]: string | number },
  ): Promise<AuthorDto> {
    return this.authorService.updateAuthorData(+id, replacementData);
  }
}
