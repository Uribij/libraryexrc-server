import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { BookDto } from './book.dto';
import { Book } from './book.interface';
import { BookService } from './book.service';

@Controller('book')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Get()
  getAllBooks(): Promise<BookDto[]> {
    return this.bookService.getAllBooks();
  }

  @Get('/:id')
  getBookById(@Param('id') id: number) {
    return this.bookService.getBookById(id);
  }

  @Delete('/:id')
  deleteBook(@Param('id') id: number): Promise<boolean> {
    return this.bookService.deleteBook(id);
  }

  @Put()
  updateBook(@Body() book: Book): Promise<BookDto> {
    return this.bookService.updateBook(book);
  }

  @Post()
  addBook(@Body() book: Book): Promise<BookDto> {
    return this.bookService.addBook(book);
  }

  @Put('/:id')
  updateBookData(
    @Param('id') id: number,
    @Body()
    replacementData: { [propertyName: string]: string | number },
  ): Promise<BookDto> {
    return this.bookService.updateBookData(+id, replacementData);
  }
}
