import { HttpException, Injectable } from '@nestjs/common';
import { SequenceService } from 'src/sequence/sequence.service';
import { UserRepository } from 'src/user/user.repository';
import { UserService } from 'src/user/user.service';
import { BookDto } from './book.dto';
import { Book } from './book.interface';
import { BookRepository } from './book.repository';

@Injectable()
export class BookService {

  constructor(
    private readonly bookRepository: BookRepository,
    private readonly sequenceService: SequenceService,
    private readonly userRepository:UserRepository,
  ) {}

  getAllBooks(): Promise<BookDto[]> {
    return this.bookRepository.getAllBooks();
  }

  updateBook(book: BookDto): Promise<BookDto> {
    return this.bookRepository.updateBook(book);
  }

  async addBook(book: BookDto): Promise<BookDto> {
    book = { ...book, bookId: await this.sequenceService.getNewId('books') };
    if (await this.bookRepository.addBook(book)) {
      return this.getBookById(await book.bookId);
    }
  }

  async deleteBook(bookId: number): Promise<boolean> {
    if (!this.verifyBook(+`${bookId}`)) {
      throw new HttpException(
        'The book you are trying to delete does not exist!',
        404,
      );
    } else {
      if(await this.bookRepository.deleteBook(+`${bookId}`)){
        this.userRepository.removeBooksFromAllLists(+`${bookId}`);
        return true;
      }
    }
  }

  getBookById(bookId: number): Promise<BookDto> {
    return this.bookRepository.getBookById(+`${bookId}`);
  }

  verifyBook(bookId: number): boolean {
    return !!this.getBookById(+`${bookId}`);
  }

  updateBookData(
    bookId: number,
    replacementData: { [propertyName: string]: string | number },
  ): Promise<BookDto> {
    if (!this.verifyBook(+`${bookId}`)) {
      throw new HttpException(
        'The user you are trying to update does not exist!',
        404,
      );
    } else {
      return this.bookRepository.updateBookData(+`${bookId}`, replacementData);
    }
  }
}
