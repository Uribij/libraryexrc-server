import { ObjectId } from 'mongodb';

export interface Book {
  _id: ObjectId;
  bookId: number;
  bookName: string;
  author: number;
}
