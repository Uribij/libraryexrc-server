import { Module } from '@nestjs/common';
import { MongoConectorModule } from 'src/mongo-conector/mongo-conector.module';
import { SequenceRepository } from 'src/sequence/sequence.repository';
import { SequenceService } from 'src/sequence/sequence.service';
import { UserRepository } from 'src/user/user.repository';
import { UserService } from 'src/user/user.service';
import { BookController } from './book.controller';
import { BookRepository } from './book.repository';
import { BookService } from './book.service';

@Module({
  controllers: [BookController],
  providers: [BookService,SequenceService,BookRepository,SequenceRepository,UserRepository],
  imports:[MongoConectorModule]
})
export class BookModule {}
