import { Inject, Injectable } from '@nestjs/common';
import { Db, ObjectId } from 'mongodb';
import { BookDto } from './book.dto';
import { Book } from './book.interface';

@Injectable()
export class BookRepository {

  constructor(
    @Inject('DATABASE_CONNECTION')
    private db: Db,
  ) {}

  private readonly COLLECTION_NAME = 'books';

  async getAllBooks(): Promise<BookDto[]> {
    return await this.db.collection(this.COLLECTION_NAME).find().toArray();
  }

  async addBook(book: BookDto): Promise<boolean> {
    return !!(await this.db.collection(this.COLLECTION_NAME).insertOne(book))
      .result.ok;
  }

  async getBookById(bookId: number):Promise<BookDto> {
    return { ... await this.db.collection(this.COLLECTION_NAME).findOne({ bookId }) };;
  }

  async updateBook(book: BookDto): Promise<BookDto> {
    return (await this.db
      .collection(this.COLLECTION_NAME)
      .findOneAndUpdate(
        { _id: book['_id'] },
        { $set: { bookName: book.bookName } },
        { returnOriginal: false },
      )).value as BookDto;
  }

  async deleteBook(bookId: number): Promise<boolean> {
    return !!(await this.db.collection(this.COLLECTION_NAME).deleteOne({ bookId }))
      .result.ok;
  }

  async updateBookData(
    bookId: number,
    pairValueSet: { [propertyName: string]: number | string },
  ): Promise<BookDto> {
    return (
      await this.db
        .collection(this.COLLECTION_NAME)
        .findOneAndUpdate(
          { bookId },
          { $set: pairValueSet },
          { returnOriginal: false },
        )
    ).value as BookDto;
  }
}
