import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthorModule } from './author/author.module';
import { BookModule } from './book/book.module';


@Module({
  controllers: [],
  providers: [],
  imports: [UserModule, AuthorModule, BookModule],
})
export class AppModule {}
