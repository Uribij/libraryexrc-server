import { Module } from '@nestjs/common';
import { Db, MongoClient } from 'mongodb'

@Module({
    providers: [
        {
            provide: 'DATABASE_CONNECTION',
            useFactory: async (): Promise<Db> => {
                try {
                    const client = await MongoClient.connect(`mongodb://${process.env.DB_SERVER_ADDRESS}`, {
                        useUnifiedTopology: true
                    });

                    console.log('connected to db')

                    return client.db(process.env.DB_NAME);
                } catch (error) {
                    throw error;
                }
            }
        }
    ],
    exports: ['DATABASE_CONNECTION']
})
export class MongoConectorModule { }
