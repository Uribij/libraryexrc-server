import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserDto } from './user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getAllUsers(): Promise<UserDto[]> {
    return this.userService.getAllUsers();
  }

  @Get('/:id')
  getUserById(@Param('id') id: number): Promise<UserDto> {
    return this.userService.getUserById(+id);
  }

  @Delete('/:id')
  deleteUser(@Param('id') id: number): Promise<boolean> {
    return this.userService.deleteUser(+id);
  }

  @Post()
  addUser(@Body() user: UserDto): Promise<UserDto> {
    return this.userService.addUser(user);
  }

  @Put()
  updateUser(@Body() user: UserDto): Promise<UserDto> {
    return this.userService.updateUser(user);
  }

  @Put('/:id')
  updateUserData(
    @Param('id') id: number,
    @Body()
    replacementData: { [propertyName: string]: string | number | number[] },
  ): Promise<UserDto> {
    return this.userService.updateUserData(+id, replacementData);
  }
}
