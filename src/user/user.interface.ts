import { ObjectId } from "mongodb";

export interface User{
    _id:ObjectId;
    userId:number;
    userName:string;
    favoriteBook:number;
    bookList:number[];
}