import { Inject, Injectable } from '@nestjs/common';
import { Db, ObjectId } from 'mongodb';
import { UserDto } from './user.dto';
import { User } from './user.interface';

@Injectable()
export class UserRepository {
  constructor(
    @Inject('DATABASE_CONNECTION')
    private db: Db,
  ) {}

  private readonly COLLECTION_NAME = 'users';

  async getAllUsers(): Promise<UserDto[]> {
    return await this.db.collection(this.COLLECTION_NAME).find({}).toArray();
  }

  async addUser(user: UserDto): Promise<boolean> {
    return !!(await this.db.collection(this.COLLECTION_NAME).insertOne(user))
      .result.ok;
  }

  async getUserById(id: number): Promise<UserDto> {
    return {
      ...(await this.db.collection(this.COLLECTION_NAME).findOne({ id })),
    };
  }

  async updateUser(user: UserDto): Promise<UserDto> {
    return (
      await this.db.collection(this.COLLECTION_NAME).findOneAndUpdate(
        { _id: user['_id'] },
        {
          $set: {
            userName: user.userName,
            bookList: user.bookList,
            favoriteBook: user.favoriteBook,
            userId: user.userId,
          },
        },
        { returnOriginal: false },
      )
    ).value as UserDto;
  }

  async deleteUser(userId: number): Promise<boolean> {
    return !!(await this.db.collection(this.COLLECTION_NAME).deleteOne({ userId }))
      .result.ok;
  }

  async updateUserData(
    userId: number,
    pairValueSet :{[propertyName:string]:number | string | number[]}
  ): Promise<UserDto> {
    return (
      await this.db
        .collection(this.COLLECTION_NAME)
        .findOneAndUpdate(
          { userId },
          { $set: pairValueSet },
          { returnOriginal: false },
        )
    ).value as UserDto;
  }

  async removeBooksFromAllLists(bookId:number){
    return !!((await this.db.collection(this.COLLECTION_NAME).updateMany({},{$pull:{bookList:bookId}})).result.ok);
  }
}
