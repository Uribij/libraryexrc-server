import { HttpException, Injectable } from '@nestjs/common';
import { UserDto } from './user.dto';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  getAllUsers(): Promise<UserDto[]> {
    return this.userRepository.getAllUsers();
  }

  getUserById(userId: number): Promise<UserDto> {
    return this.userRepository.getUserById(+`${userId}`);
  }

  addUser(user: UserDto): Promise<UserDto> {
    if (this.verifyUser(user.userId)) {
      throw new HttpException('A user with this ID already exists!', 404);
    } else {
      if (!this.userRepository.addUser(user)) {
        throw new HttpException('Invalid user details!', 406);
      }
      return this.getUserById(user.userId);
    }
  }

  deleteUser(userId: number): Promise<boolean> {
    if (!this.verifyUser(+`${userId}`)) {
      throw new HttpException(
        'The user you are trying to delete does not exist',
        404,
      );
    } else {
      return this.userRepository.deleteUser(+`${userId}`);
    }
  }

  updateUser(user: UserDto) {
    if (!this.verifyUser(user.userId)) {
      throw new HttpException(
        'The user you are trying to update does not exist!',
        404,
      );
    } else {
      return this.userRepository.updateUser(user);
    }
  }

  verifyUser(id: number): boolean {
    return this.getUserById(id) ? true : false;
  }

  updateUserData(
    userId: number,
    replacementData: { [propertyName: string]: string | number | number[] },
  ): Promise<UserDto> {
    if (!this.verifyUser(+`${userId}`)) {
      throw new HttpException(
        'The user you are trying to update does not exist!',
        404,
      );
    } else {
      return this.userRepository.updateUserData(+`${userId}`, replacementData);
    }
  }
}
