import { Module } from '@nestjs/common';
import { MongoConectorModule } from 'src/mongo-conector/mongo-conector.module';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';

@Module({
  controllers: [UserController],
  providers: [UserService,UserRepository],
  imports:[MongoConectorModule]
})
export class UserModule {}
