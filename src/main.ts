require('dotenv').config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT || 3000;
  const options = {
    "origin": "http://localhost:4200",
    "methods": "GET,HEAD,PUT,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204,
    "credentials":true
}
app.enableCors(options)
  await app.listen(port);
}
bootstrap();
