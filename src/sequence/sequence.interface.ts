import {ObjectId} from "mongodb";

export interface Sequence{
    _id:ObjectId;
    name:string;
    current:number;
}