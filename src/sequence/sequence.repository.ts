import { Inject, Injectable } from '@nestjs/common';
import { Db } from 'mongodb';
import { SequenceDto } from './sequence.dto';
import { Sequence } from './sequence.interface';

@Injectable()
export class SequenceRepository {
  constructor(
    @Inject('DATABASE_CONNECTION')
    private db: Db,
  ) {}

  private readonly COLLECTION_NAME = 'sequences';

  async getSequence(sequenceName: string): Promise<Sequence> {
    return await this.db
      .collection(this.COLLECTION_NAME)
      .findOne({ name: `${sequenceName}` });
  }

  addSequence(sequence: SequenceDto): void {
    this.db.collection(this.COLLECTION_NAME).insertOne(sequence);
  }

  async sequenceIncrease(sequence: Sequence): Promise<void> {
    await this.db
      .collection(this.COLLECTION_NAME)
      .updateOne({ _id: sequence._id }, { $inc: { current: 1 } });
  }
}
