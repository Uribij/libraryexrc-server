import { Injectable } from '@nestjs/common';
import { SequenceDto } from './sequence.dto';
import { Sequence } from './sequence.interface';
import { SequenceRepository } from './sequence.repository';

@Injectable()
export class SequenceService {
  constructor(private readonly sequenceRepository: SequenceRepository) {}

  async getNewId(sequenceName: string): Promise<number> {
    const sequence: SequenceDto = await this.sequenceRepository.getSequence(
      sequenceName,
    );
    let newId = 1;
    if (!sequence) {
      this.sequenceRepository.addSequence({
        name: sequenceName,
        current: 0,
      } as SequenceDto);
    } else {
      newId = sequence.current + 1;
      await this.sequenceRepository.sequenceIncrease(sequence as Sequence);
    }
    return newId;
  }
}
